/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/1/25 17:29:47                           */
/*==============================================================*/


drop table if exists sys_config;

drop table if exists sys_dept;

drop table if exists sys_dict_data;

drop table if exists sys_dict_type;

drop table if exists sys_job;

drop table if exists sys_job_log;

drop table if exists sys_logininfor;

drop table if exists sys_menu;

drop table if exists sys_notice;

drop table if exists sys_oper_log;

drop table if exists sys_post;

drop table if exists sys_role;

drop table if exists sys_role_dept;

drop table if exists sys_role_menu;

drop table if exists sys_user;

drop table if exists sys_user_online;

drop table if exists sys_user_post;

drop table if exists sys_user_role;

/*==============================================================*/
/* Table: sys_config                                            */
/*==============================================================*/
create table sys_config
(
   config_id            int(5) not null auto_increment comment '字典编码',
   config_name          varchar(100) default '0' comment '字典排序',
   config_key           varchar(100) default '' comment '字典标签',
   config_value         varchar(100) default '' comment '字典键值',
   config_type          char(1) default '' comment '字典类型',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注',
   primary key (config_id)
);

alter table sys_config comment '参数配置表';

/*==============================================================*/
/* Table: sys_dept                                              */
/*==============================================================*/
create table sys_dept
(
   dept_id              int(11) not null auto_increment comment '部门id',
   parent_id            int(11) default 0 comment '父部门id',
   ancestors            varchar(50),
   dept_name            varchar(30) default '' comment '部门名称',
   order_num            int(4) default 0 comment '显示顺序',
   leader               varchar(20) default '' comment '负责人',
   phone                varchar(20) default '' comment '联系电话',
   email                varchar(20) default '' comment '邮箱',
   status               char(1) default '0' comment '部门状态:0正常,1停用',
   del_flag             char(1),
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   primary key (dept_id)
);

alter table sys_dept comment '部门表';

/*==============================================================*/
/* Table: sys_dict_data                                         */
/*==============================================================*/
create table sys_dict_data
(
   dict_code            int(11) not null auto_increment comment '字典编码',
   dict_sort            int(4) default 0 comment '字典排序',
   dict_label           varchar(100) default '' comment '字典标签',
   dict_value           varchar(100) default '' comment '字典键值',
   dict_type            varchar(100) default '' comment '字典类型',
   css_class            varchar(100),
   list_class           varchar(100),
   is_default           char(1),
   status               int(1) default 0 comment '状态（0正常 1禁用）',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注',
   primary key (dict_code)
);

alter table sys_dict_data comment '字典数据表';

/*==============================================================*/
/* Table: sys_dict_type                                         */
/*==============================================================*/
create table sys_dict_type
(
   dict_id              int(11) not null auto_increment comment '字典主键',
   dict_name            varchar(100) default '' comment '字典名称',
   dict_type            varchar(100) default '' comment '字典类型',
   status               char(1) default '0' comment '状态（0正常 1禁用）',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注',
   unique               (dict_type),
   primary key (dict_id)
);

alter table sys_dict_type comment '字典类型表';

/*==============================================================*/
/* Table: sys_job                                               */
/*==============================================================*/
create table sys_job
(
   job_id               int(11) not null auto_increment comment '任务ID',
   job_name             varchar(64) not null default '' comment '任务名称',
   job_group            varchar(64) not null default '' comment '任务组名',
   method_name          varchar(500) default '' comment '任务方法',
   method_params        varchar(200) default '' comment '方法参数',
   cron_expression      varchar(255) default '' comment 'cron执行表达式',
   misfire_policy       varchar(20),
   status               int(1) default 0 comment '状态（0正常 1暂停）',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注信息',
   primary key (job_id, job_name, job_group)
);

alter table sys_job comment '定时任务调度表';

/*==============================================================*/
/* Table: sys_job_log                                           */
/*==============================================================*/
create table sys_job_log
(
   job_log_id           int(11) not null auto_increment comment '任务日志ID',
   job_name             varchar(64) not null comment '任务名称',
   job_group            varchar(64) not null comment '任务组名',
   method_name          varchar(500) comment '任务方法',
   method_params        varchar(200) default '' comment '方法参数',
   job_message          varchar(500) comment '日志信息',
   is_exception         char(1) default '0' comment '是否异常',
   exception_info       text comment '异常信息',
   create_time          timestamp comment '创建时间',
   primary key (job_log_id)
);

alter table sys_job_log comment '定时任务调度日志表';

/*==============================================================*/
/* Table: sys_logininfor                                        */
/*==============================================================*/
create table sys_logininfor
(
   info_id              int(11) not null auto_increment comment '访问ID',
   login_name           varchar(50) default '' comment '登录账号',
   ipaddr               varchar(50) default '' comment '登录IP地址',
   login_location       varchar(255),
   browser              varchar(50) default '' comment '浏览器类型',
   os                   varchar(50) default '' comment '操作系统',
   status               int(1) default 0 comment '登录状态 0成功 1失败',
   msg                  varchar(255) default '' comment '提示消息',
   login_time           timestamp comment '访问时间',
   primary key (info_id)
);

alter table sys_logininfor comment '系统访问记录';

/*==============================================================*/
/* Table: sys_menu                                              */
/*==============================================================*/
create table sys_menu
(
   menu_id              int(11) not null auto_increment comment '菜单ID',
   menu_name            varchar(50) not null comment '菜单名称',
   parent_id            int(11) default 0 comment '父菜单ID',
   order_num            int(4) default NULL comment '显示顺序',
   url                  varchar(200) default '' comment '请求地址',
   menu_type            char(1) default '' comment '类型:M目录,C菜单,F按钮',
   visible              char(1) default '0' comment '菜单状态:0显示,1隐藏',
   perms                varchar(100) default '' comment '权限标识',
   icon                 varchar(100) default '' comment '菜单图标',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注',
   primary key (menu_id)
);

alter table sys_menu comment '菜单权限表';

/*==============================================================*/
/* Table: sys_notice                                            */
/*==============================================================*/
create table sys_notice
(
   notice_id            int(4) not null auto_increment comment '任务日志ID',
   notice_title         varchar(50) not null comment '任务名称',
   notice_type          char(2) not null comment '任务组名',
   notice_content       varchar(500) comment '任务方法',
   status               char(1) default '' comment '方法参数',
   create_by            varchar(64) comment '日志信息',
   create_time1         datetime default '0' comment '是否异常',
   update_by            varchar(64) comment '异常信息',
   update_time1         datetime,
   remark               varchar(255) comment '创建时间',
   primary key (notice_id)
);

alter table sys_notice comment '通知公告表';

/*==============================================================*/
/* Table: sys_oper_log                                          */
/*==============================================================*/
create table sys_oper_log
(
   oper_id              int(11) not null auto_increment comment '日志主键',
   title                varchar(50) default '' comment '模块标题',
   business_type        int(2) default '' comment '功能请求',
   method               varchar(100) default '' comment '方法名称',
   operator_type        int(1) default '' comment '来源渠道',
   oper_name            varchar(50) default '' comment '登录账号',
   dept_name            varchar(50) default '' comment '部门名称',
   oper_url             varchar(255) default '' comment '请求URL',
   oper_ip              varchar(30) default '' comment '主机地址',
   oper_param           varchar(255) default '' comment '请求参数',
   status               int(1) default 0 comment '操作状态 0正常 1异常',
   error_msg            varchar(2000) default '' comment '错误消息',
   oper_time            timestamp comment '操作时间',
   primary key (oper_id)
);

alter table sys_oper_log comment '操作日志记录';

/*==============================================================*/
/* Table: sys_post                                              */
/*==============================================================*/
create table sys_post
(
   post_id              int(11) not null auto_increment comment '岗位ID',
   post_code            varchar(64) not null comment '岗位编码',
   post_name            varchar(100) not null comment '岗位名称',
   post_sort            int(4) not null comment '显示顺序',
   status               char(1) not null comment '状态（0正常 1停用）',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注',
   primary key (post_id)
);

alter table sys_post comment '岗位信息表';

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   role_id              int(10) not null auto_increment comment '角色ID',
   role_name            varchar(30) not null comment '角色名称',
   role_key             varchar(100) not null comment '角色权限字符串',
   role_sort            int(10) comment '显示顺序',
   data_scope           char(1),
   status               char(1) default '0' comment '角色状态:0正常,1禁用',
   del_flag             char(1),
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500) default '' comment '备注',
   primary key (role_id)
);

alter table sys_role comment '角色信息表';

/*==============================================================*/
/* Table: sys_role_dept                                         */
/*==============================================================*/
create table sys_role_dept
(
   role_id              int(11) not null comment '用户ID',
   dept_id              int(11) not null comment '岗位ID',
   primary key (role_id, dept_id)
);

alter table sys_role_dept comment '角色和部门关联表';

/*==============================================================*/
/* Table: sys_role_menu                                         */
/*==============================================================*/
create table sys_role_menu
(
   role_id              int(11) not null comment '角色ID',
   menu_id              int(11) not null comment '菜单ID',
   primary key (role_id, menu_id)
);

alter table sys_role_menu comment '角色和菜单关联表';

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   user_id              int(11) not null auto_increment comment '用户ID',
   dept_id              int(11) default NULL comment '部门ID',
   login_name           varchar(30) default '' comment '登录账号',
   user_name            varchar(30) default '' comment '用户昵称',
   user_type            char(1) default 'N' comment '类型:Y默认用户,N非默认用户',
   email                varchar(100) default '' comment '用户邮箱',
   phonenumber          varchar(20) default '' comment '手机号码',
   sex                  char(1),
   avatar               varchar(100),
   password             varchar(100) default '' comment '密码',
   salt                 varchar(100) default '' comment '盐加密',
   status               char(1) default '0' comment '帐号状态:0正常,1禁用',
   del_flag             char(1) default '' comment '拒绝登录描述',
   create_by            varchar(64) default '' comment '创建者',
   create_time          timestamp comment '创建时间',
   update_by            varchar(64) default '' comment '更新者',
   update_time          timestamp comment '更新时间',
   remark               varchar(500),
   primary key (user_id)
);

alter table sys_user comment '用户信息表';

/*==============================================================*/
/* Table: sys_user_online                                       */
/*==============================================================*/
create table sys_user_online
(
   sessionId            varchar(50) not null default '' comment '用户会话id',
   login_name           varchar(50) default '' comment '登录账号',
   dept_name            varchar(50) default '' comment '部门名称',
   ipaddr               varchar(50) default '' comment '登录IP地址',
   login_location       varchar(255),
   browser              varchar(50) default '' comment '浏览器类型',
   os                   varchar(50) default '' comment '操作系统',
   status               varchar(10) default '' comment '在线状态on_line在线off_line离线',
   start_timestamp      timestamp comment 'session创建时间',
   last_access_time     timestamp comment 'session最后访问时间',
   expire_time          int(5) default 0 comment '超时时间，单位为分钟',
   primary key (sessionId)
);

alter table sys_user_online comment '在线用户记录';

/*==============================================================*/
/* Table: sys_user_post                                         */
/*==============================================================*/
create table sys_user_post
(
   user_id              int(11) not null comment '用户ID',
   post_id              int(11) not null comment '岗位ID',
   primary key (user_id, post_id)
);

alter table sys_user_post comment '用户与岗位关联表';

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
create table sys_user_role
(
   user_id              int(11) not null comment '用户ID',
   role_id              int(11) not null comment '角色ID',
   primary key (user_id, role_id)
);

alter table sys_user_role comment '用户和角色关联表';

